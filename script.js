function createNewUser() {
    const firstName = prompt('Enter your name:');
    const lastName = prompt('Enter your surname:');
    const birthdayString = prompt('Enter your date of birth (dd.mm.yyyy):');
    const birthday = new Date(birthdayString.split('.').reverse().join('-'));

    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getAge: function() {
            const now = new Date();
            const ageInMs = now - this.birthday;
            const ageInYears = ageInMs / 1000 / 60 / 60 / 24 / 365.25;
            return Math.floor(ageInYears);
        },
        getPassword: function() {
            const firstLetter = this.firstName.charAt(0).toUpperCase();
            const lastNameLower = this.lastName.toLowerCase();
            const year = this.birthday.getFullYear();
            return firstLetter + lastNameLower + year;
        }
    };

    return newUser;
}

const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
